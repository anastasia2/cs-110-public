# Notes, etc for CS-110

This is public so feel free. Will add v. basic git guide if anybody wants also (not an expert though)

Paper is soooooo 2016

#### Process notes

I used [typora](https://typora.io) to make this after switching to win10 for this class. Before I maintained a markdown folder with notes and converted it to html using [pandoc](pandoc.org) and some [bomb-ass stylesheets.](https://github.com/otsaloma/markdown-css) Typora will eventually stop being free but I suspect this class will end long before that. In case it does, I'll find some way to add typora's diagram JS stuff to my previous setup and do that again.